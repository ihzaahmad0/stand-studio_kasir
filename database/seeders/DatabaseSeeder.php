<?php

namespace Database\Seeders;

use App\Models\role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $roles = ['Admin' => [0, 0], 'VVIP' => [0, 9999], 'VIP A' => [0, 249], 'VIP B' => [250, 499], 'Gold' => [500, 749], 'Silver' => [750, 999], 'Bronze' => [1000, 1249]];
        foreach ($roles as $role => $minMax) {
            DB::table('roles')->insert([
                'name' => $role,
                'min' => $minMax[0],
                'max' => $minMax[1]
            ]);
        }

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'phone' => '012345678',
            'password' => Hash::make('123'),
            'city_of_birth' => 'Bpp',
            'birth' => Carbon::now(),
            'role_id' => 1
        ]);

        $specialCode = "0001 0002 0003 0005 0006 0007 0008 0008 0009 1000 2000 3000 5000 6000 7000 8000 9000 10000 1111 2222 3333 5555 6666 7777 8888 9999 1122 1133 1155 1166 1177 1188 1199 0011 0022 0033 0055 0066 0077 0088 0099 2233 2255 2266 2277 2288 2299 3355 3366 3377 3388 3399 4455 4466 4477 4488 4499 5566 5577 5588 5599 6677 6688 6699 7788 7799 8899 2211 3311 4411 5511 6611 7711 8811 9911 3322 4422 5522 6622 7722 8822 9922 4433 5533 6633 7733 8833 9933 6655 7755 8855 9955 7766 8866 9966 8877 9977 0111 0222 0333 0555 0666 0777 0888 0999 9988 1222 1333 1555 1666 1777 1888 1999 2333 2555 2666 2777 2888 2999 3555 3666 3777 3888 3999 4555 4666 4777 4888 4999 5666 5777 5888 5999 6777 6888 6999 7888 7999 8999 2221 3331 5551 6661 7771 8881 9991 3332 5552 6662 7772 8882 9992 2223 5553 6663 7773 8883 9993 5554 6664 7774 8884 9994 2225 3335 6665 7775 8885 9995 2226 3336 5556 7776 8886 9996 2227 3337 5557 6667 8887 9997 2228 3338 5558 6668 7778 9998 2229 3339 5559 6669 7779 8889 0123 0234 0345 0456 0567 0678 0789 8910 1123 1234 1345 1456 1567 1678 1789 2123 2234 2345 2456 2567 2678 2789 3123 3234 3345 3456 3567 3678 3789 4123 4234 4345 4456 4567 4678 4789 5123 5234 5345 5456 5567 5678 5789 6123 6234 6345 6456 6567 6678 6789 7123 7234 7345 7456 7567 7678 7789 8123 8234 8345 8456 8567 8678 8789 9123 9234 9345 9456 9567 9678 9789 5758 5711 5722 5733 5755 5766 5777 5788 5799";
        $specialCode = array_unique(explode(" ", $specialCode));
        foreach ($specialCode as $sc) {
            $minMaxKode = role::whereNotIn('id', [1, 2])->get()->keyBy('id');
            $isValueExist = false;
            foreach ($minMaxKode as $item) {
                if ($isValueExist) {
                    $item->min++;
                    $item->max++;
                    $item->save();
                }
                if (in_array($sc, range($item->min, $item->max)) && !$isValueExist) {
                    $item->max++;
                    $item->special_code++;
                    $item->save();
                    $isValueExist = true;
                }
            }
            DB::table('special_codes')->insert(['kode' => $sc, 'created_at' => Carbon::now()]);
        }
    }
}
