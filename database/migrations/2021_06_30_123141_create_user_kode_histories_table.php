<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserKodeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_kode_histories', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->unsignedBigInteger('source_user_id');
            $table->foreign('source_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('destination_user_id');
            $table->foreign('destination_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_kode_histories');
    }
}
