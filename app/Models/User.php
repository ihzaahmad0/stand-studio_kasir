<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use HasFactory;
    protected $guarded = [];
    protected $hidden = ['id', 'password'];

    public function kodeHistory()
    {
        return $this->hasMany(UserKodeHistory::class);
    }

    public function role()
    {
        return $this->belongsTo(role::class);
    }

    public function getBirthAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function getKtpAttribute($value)
    {
        return asset(Storage::url($value));
    }
}
