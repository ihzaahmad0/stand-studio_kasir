<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard('Admin')->check()) {
            return redirect(route('index'));
        } else if (Auth::guard('VVIP')->check() || Auth::guard('VIP A')->check() || Auth::guard('VIP B')->check() || Auth::guard('Gold')->check() || Auth::guard('Silver')->check() || Auth::guard('Bronze')->check()) {
            return redirect(route('guard.user.index'));
        }

        return $next($request);
    }
}
