<?php

namespace App\Http\Controllers;

use App\Models\role;
use App\Models\User;
use Illuminate\Http\Request;

class indexController extends Controller
{
    public function index()
    {
        $data['users'] = User::where('role_id', '!=', 1)->orderBy('updated_at', 'desc')->with('role')->get();
        $data['roles'] = role::where('id', '!=', '1')->get();
        return view('index', compact('data'));
    }
}
