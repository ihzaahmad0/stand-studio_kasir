<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class historyController extends Controller
{
    public function index()
    {
        $data['items'] = DB::select('SELECT ukh.kode, ukh.created_at, ud.name as destination, us.name as source FROM user_kode_histories as ukh JOIN users as ud ON ud.id = ukh.destination_user_id JOIN users as us ON us.id = ukh.source_user_id');
        return view('history', compact('data'));
    }
}
