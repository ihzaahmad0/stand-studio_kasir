<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\role;
use App\Models\SpecialCode;
use App\Models\User;
use App\Models\UserKodeHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private function checkKodeBetween($kode, $level)
    {
        $minMaxKode = role::where('id', '!=', 1)->get()->keyBy('id');
        return (in_array($kode, range((int)$minMaxKode[$level]->min, $minMaxKode[$level]->max)));
    }

    private function checkSpecialCode($kode)
    {
        $specialCode = SpecialCode::pluck('kode')->toArray();
        return (in_array($kode, $specialCode));
    }

    public function store(StoreUserRequest $request)
    {
        if (!$this->checkKodeBetween($request->kode, $request->role_id)) {
            return back()->withErrors(['kode' => 'Kode tidak sesuai dengan batasan level'])
                ->withInput();
        }

        if ($request->role_id != '2' && $this->checkSpecialCode($request->kode)) {
            return back()->withErrors(['kode' => 'Kode hanya dapat digunakan oleh user VVIP'])
                ->withInput();
        }
        $data = $request->except('kode', 'ktp', 'birth');
        $KTPpath = $request->file('ktp')->store('public/images/ktp');
        $data['ktp'] = $KTPpath;
        $data['kode'] = str_pad($request->kode, 4, '0', STR_PAD_LEFT);
        $data['birth'] = Carbon::createFromFormat('d/m/Y', $request->birth)->format('Y-m-d H:i:s');
        $data['password'] = Hash::make('123');
        User::create($data);

        return back()->with('icon', 'success')->with('title', 'Berhasil!')->with('text', 'Berhasil menambahkan user.');
    }

    public function getDetail($id)
    {
        $data['user'] = User::find($id);
        if ($data['user'] == []) {
            return response('Not Found!', 400);
        }
        return response()->json($data);
    }

    public function update($id, Request $request)
    {
        Session::flash('condition', 'updateUser');
        Session::flash('updatedId', $id);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'role_id' => 'required',
            'kode' => 'required|unique:users,kode,' . $id,
            'phone' => 'required|numeric',
            'city_of_birth' => 'required',
            'birth' => 'required',
            'ktp' => 'image'
        ], [
            'name.required' => 'Kolom Nama Harus Diisi',
            'email.required' => 'Kolom Email Harus Diisi',
            'email.email' => 'Format Email Tidak Benar',
            'email.unique' => 'Email sudah digunakan',
            'role_id.required' => 'Level Harus Dipilih',
            'kode.required' => 'Kolom Kode Harus Diisi',
            'kode.unique' => 'Kolom Sudah Digunakan',
            'phone.required' => 'Kolom Nomer Telpon Harus Diisi',
            'phone.numeric' => 'Kolom Nomer Telpon Wajib Angka',
            'city_of_birth.required' => 'Kolom Tempat Lahir Wajib Diisi',
            'birth.required' => 'Kolom Tanggal Lahir Wajib Diisi',
            'ktp.image' => 'KTP Wajib Berupa Gambar'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if (!$this->checkKodeBetween($request->kode, $request->role_id)) {
            return back()->withErrors(['kode' => 'Kode tidak sesuai dengan batasan level'])
                ->withInput();
        }

        if ($request->role_id != '2' && $this->checkSpecialCode($request->kode)) {
            return back()->withErrors(['kode' => 'Kode hanya dapat digunakan oleh user VVIP'])
                ->withInput();
        }

        $user = User::find($id);
        $data = $request->except('kode', 'ktp', 'birth');
        if ($request->file('ktp') != null) {
            Storage::delete($user->getRawOriginal('ktp'));
            $KTPpath = $request->file('ktp')->store('public/images/ktp');
            $data['ktp'] = $KTPpath;
        }
        $data['kode'] = str_pad($request->kode, 4, '0', STR_PAD_LEFT);
        $data['birth'] = Carbon::createFromFormat('d/m/Y', $request->birth)->format('Y-m-d H:i:s');

        User::find($id)->update($data);
        return back()->with('icon', 'success')->with('title', 'Berhasil!')->with('text', 'Berhasil merubah data user.');
    }

    public function delete($id)
    {
        $user = User::find($id);
        Storage::delete($user->getRawOriginal('ktp'));
        $user->delete();
        return back()->with('icon', 'success')->with('title', 'Berhasil!')->with('text', 'Berhasil menghapus user.');
    }

    public function transfer(Request $request)
    {
        Session::flash('condition', 'transfer');
        $validator = Validator::make($request->all(), [
            'source' => 'required',
            'destination' => 'required',
            'kode' => 'required'
        ], [
            'source.required' => 'Kolom Nama Harus Diisi',
            'destionation.required' => 'Kolom Email Harus Diisi',
            'kode.required' => 'Kolom Kode Harus Diisi',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $source = User::find($request->source);
        $destination = User::find($request->destination);

        if (!$this->checkKodeBetween($source->kode, $destination->role_id)) {
            return back()->withErrors(['destination' => 'Kode sumber tidak sesuai dengan batasan level user tujuan'])
                ->withInput();
        }
        if (!$this->checkKodeBetween($request->kode, $source->role_id)) {
            return back()->withErrors(['kode' => 'Kode tidak sesuai dengan batasan level user sumber'])
                ->withInput();
        }

        if ($source->role_id != '2' && $this->checkSpecialCode($request->kode)) {
            return back()->withErrors(['kode' => 'Kode hanya dapat digunakan oleh user VVIP'])
                ->withInput();
        }

        if ($destination->role_id != '2' && $this->checkSpecialCode($source->kode)) {
            return back()->withErrors(['destination' => 'Kode hanya dapat digunakan oleh user VVIP'])
                ->withInput();
        }

        $isKodeUnique = User::whereKode($request->kode)->where('id', '!=', $destination->id)->count();

        if ($isKodeUnique == 0) {
            $old_source_code = $source->kode;
            $source->kode = 'temp';
            $source->save();
            UserKodeHistory::create([
                'kode' => $destination->kode,
                'source_user_id' => $source->id,
                'destination_user_id' => $destination->id
            ]);
            $destination->kode = $old_source_code;
            $source->kode = $request->kode;
            $destination->save();
            $source->save();

            return back()->with('icon', 'success')->with('title', 'Berhasil!')->with('text', 'Berhasil melakukan transfer kode user.');
        } else {
            return back()->withErrors(['kode' => 'Kode sudah digunakan'])
                ->withInput();
        }
    }
}
