<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $data = [];
        return view('user.index', compact('data'));
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'phone' => 'required|numeric',
            'city_of_birth' => 'required',
            'birth' => 'required',
            'ktp' => 'image'
        ], [
            'name.required' => 'Kolom Nama Harus Diisi',
            'email.required' => 'Kolom Email Harus Diisi',
            'email.email' => 'Format Email Tidak Benar',
            'email.unique' => 'Email sudah digunakan',
            'phone.required' => 'Kolom Nomer Telpon Harus Diisi',
            'phone.numeric' => 'Kolom Nomer Telpon Wajib Angka',
            'city_of_birth.required' => 'Kolom Tempat Lahir Wajib Diisi',
            'birth.required' => 'Kolom Tanggal Lahir Wajib Diisi',
            'ktp.image' => 'KTP Wajib Berupa Gambar'
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find(Auth::user()->id);
        $data = $request->except('ktp', 'birth');
        $data['birth'] = Carbon::createFromFormat('d/m/Y', $request->birth)->format('Y-m-d H:i:s');
        if ($request->hasFile('ktp')) {
            Storage::delete($user->getRawOriginal('ktp'));
            $KTPpath = $request->file('ktp')->store('public/images/ktp');
            $data['ktp'] = $KTPpath;
        }
        User::find($user->id)->update($data);
        return back()->with('icon', 'success')->with('title', 'Berhasil!')->with('text', 'Berhasil merubah data user.');
    }
}
