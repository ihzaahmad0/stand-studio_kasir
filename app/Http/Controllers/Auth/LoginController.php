<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function loginGet()
    {
        return view('auth.login');
    }

    public function loginPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $user = User::where('email', $request->email)->first();
        if ($user != []) {
            if (Hash::check($request->password, $user->password)) {
                $remember = $request->has('remember') ? true : false;
                Auth::guard($user->role->name)->loginUsingId($user->id, $remember);
                if ($user->role->name == 'Admin') {
                    return redirect()->intended(route('index'));
                } else {
                    return redirect()->intended(route('guard.user.index'));
                }
            } else {
                return back()->with('icon', 'error')->with('title', 'Maaf')->with('text', 'username atau password salah!');
            }
        } else {
            return back()->with('icon', 'error')->with('title', 'Maaf')->with('text', 'username atau password salah!');
        }
    }

    public function logout()
    {
        if (Auth::guard('Admin')->check()) {
            Auth::guard('Admin')->logout();
        } else if (Auth::guard('VVIP')->check()) {
            Auth::guard('VVIP')->logout();
        } else if (Auth::guard('VIP A')->check()) {
            Auth::guard('VIP A')->logout();
        } else if (Auth::guard('VIP B')->check()) {
            Auth::guard('VIP B')->logout();
        } else if (Auth::guard('Gold')->check()) {
            Auth::guard('Gold')->logout();
        } else if (Auth::guard('Silver')->check()) {
            Auth::guard('Silver')->logout();
        } else if (Auth::guard('Bronze')->check()) {
            Auth::guard('Bronze')->logout();
        }
        return redirect(route('login.get'));
    }
}
