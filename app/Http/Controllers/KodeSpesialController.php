<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSpecialCodeRequest;
use App\Models\role;
use App\Models\SpecialCode;
use Illuminate\Http\Request;

class KodeSpesialController extends Controller
{
    public function index()
    {
        $data['codes'] = SpecialCode::orderBy('kode', 'asc')->get();
        $data['roles'] = role::whereNotIn('id', [1, 2])->get();
        return view('specialCode', compact('data'));
    }

    public function store(StoreSpecialCodeRequest $request)
    {
        $kode = SpecialCode::where('kode', str_pad($request->kode, 4, '0', STR_PAD_LEFT))->count();
        if ($kode > 0) {
            return back()->withErrors(['kode' => 'Kode Sudah Digunakan'])
                ->withInput();
        }

        $minMaxKode = role::whereNotIn('id', [1, 2])->get()->keyBy('id');
        $isValueExist = false;
        foreach ($minMaxKode as $item) {
            if ($isValueExist) {
                $item->min++;
                $item->max++;
                $item->save();
            }
            if (in_array($request->kode, range($item->min, $item->max)) && !$isValueExist) {
                $item->max++;
                $item->special_code++;
                $item->save();
                $isValueExist = true;
            }
        }
        SpecialCode::create([
            'kode' => str_pad($request->kode, 4, '0', STR_PAD_LEFT)
        ]);

        return back()->with('icon', 'success')->with('title', 'Berhasil!')->with('text', 'Berhasil menambahkan kode spesial.');
    }

    public function delete($id)
    {
        SpecialCode::find($id)->delete();

        return back()->with('icon', 'success')->with('title', 'Berhasil!')->with('text', 'Berhasil menghapus kode spesial.');
    }
}
