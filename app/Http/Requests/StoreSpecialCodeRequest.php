<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class StoreSpecialCodeRequest extends FormRequest
{

    protected function prepareForValidation()
    {
        Session::flash('condition', 'storeSpecialCode');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode' => 'required|unique:special_codes'
        ];
    }

    public function messages()
    {
        return [
            'kode.required' => 'Kolom Kode Harus Diisi',
            'kode.unique' => 'Kode Sudah Digunakan'
        ];
    }
}
