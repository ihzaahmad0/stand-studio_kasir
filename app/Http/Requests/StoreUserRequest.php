<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        Session::flash('condition', 'storeUser');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'role_id' => 'required',
            'kode' => 'required|unique:users',
            'phone' => 'required|numeric',
            'city_of_birth' => 'required',
            'birth' => 'required',
            'ktp' => 'required|image'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Kolom Nama Harus Diisi',
            'email.required' => 'Kolom Email Harus Diisi',
            'email.email' => 'Format Email Tidak Benar',
            'email.unique' => 'Email sudah digunakan',
            'role_id.required' => 'Level Harus Dipilih',
            'kode.required' => 'Kolom Kode Harus Diisi',
            'kode.unique' => 'Kode Sudah Digunakan',
            'phone.required' => 'Kolom Nomer Telpon Harus Diisi',
            'phone.numeric' => 'Kolom Nomer Telpon Wajib Angka',
            'city_of_birth.required' => 'Kolom Tempat Lahir Wajib Diisi',
            'birth.required' => 'Kolom Tanggal Lahir Wajib Diisi',
            'ktp.required' => 'KTP Wajib Diisi',
            'ktp.image' => 'KTP Wajib Berupa Gambar'
        ];
    }
}
