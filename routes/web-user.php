<?php

use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('user')->name('guard.user.')->middleware('auth:VVIP,VIP A,VIP B,Gold,Silver,Bronze')->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('index');
    Route::post('update-profile', [UserController::class, 'update'])->name('update');
});
