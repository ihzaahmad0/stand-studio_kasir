<?php

use App\Http\Controllers\historyController;
use App\Http\Controllers\indexController;
use App\Http\Controllers\KodeSpesialController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->middleware('auth:Admin')->group(function () {
    Route::get('/', [indexController::class, 'index'])->name('index');
    Route::name('user.')->prefix('user')->group(function () {
        Route::post('store', [UserController::class, 'store'])->name('store');
        Route::get('{id}', [UserController::class, 'getDetail'])->name('detail');
        Route::post('update/{id}', [UserController::class, 'update'])->name('update');
        Route::get('delete/{id}', [UserController::class, 'delete'])->name('delete');
        Route::post('transfer', [UserController::class, 'transfer'])->name('transfer');
    });

    Route::name('kode.')->prefix('kode')->group(function () {
        Route::resource('/', KodeSpesialController::class)->only(['index', 'store']);
        Route::get('delete/{id}', [KodeSpesialController::class, 'delete'])->name('delete');
    });

    Route::get('history', [historyController::class, 'index'])->name('history.index');
});
