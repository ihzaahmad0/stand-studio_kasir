<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\historyController;
use App\Http\Controllers\indexController;
use App\Http\Controllers\KodeSpesialController;
use App\Http\Controllers\TransferCodeController;
use App\Http\Controllers\UserController;
use App\Models\UserKodeHistory;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('generate', function () {
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    echo 'ok';
});

Route::get('/clear-cache', function () {
    \Illuminate\Support\Facades\Artisan::call('optimize:clear');
    echo 'ok';
});

Route::get('login', [LoginController::class, 'loginGet'])->name('login.get')->middleware('guest');
Route::post('login', [LoginController::class, 'loginPost'])->name('login.post')->middleware('guest');
Route::get('logout', [LoginController::class, 'logout'])->name('logout');

Route::get('', function () {
    return redirect(route('login.get'));
});
