@extends('master')

@section('css_after')
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
    <div class="container-fluid my-4">
        <div class="row mb-4 d-flex">
            <div class="col-md-8 mx-auto">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <h1 class="text-center">Rangkuman Data</h1>
                    </div>
                    @foreach ($data['roles'] as $item)
                        <div class="col-md-4 mb-4 shadow ">
                            <div class="card text-center shadow ">
                                <div class="card-body">
                                    <h4 class="card-title">{{ $item->special_code }}</h4>
                                    <p class="card-text">Kode spesial pada level {{ $item->name }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <hr>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-header d-flex">
                        <h2>List Kode Spesial</h2>
                        <button class="btn btn-sm btn-primary ml-auto" id="add_button"><i class="fas fa-plus"></i>
                            Tambah</button>
                    </div>
                    <div class="card-body">
                        <table id="main_table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['codes'] as $code)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $code->kode }}</td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <button class="btn btn-sm btn-danger btn_delete"
                                                    data-id="{{ $code->id }}" data-toggle="tooltip" data-placement="top"
                                                    title="Hapus Data">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="main_modal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="main_form" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="kode">Kode</label>
                            <input type="number" class="form-control @error('kode') is-invalid @enderror" id="kode"
                                name="kode" placeholder="Masukkan kode spesial (0 - 9999)" value="{{ old('kode') }}">
                            @error('kode')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
        integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        const URL = {
            add: "{{ route('kode.store') }}",
            delete: "{{ route('kode.delete', ['__id']) }}"
        }
    </script>

    <script>
        function openModal(data) {
            $("#main_form").attr('action', data.url)
            $("#modal_title").html(data.title)
            $("#main_modal").modal('show')
        }
        $(document).ready(function() {
            $('#main_table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            })

            $("#kode").mask('9999');

            $("#add_button").click(function() {
                openModal({
                    url: URL.add,
                    title: 'Tambah Kode Spesial'
                })
            })

            $(document).on('click', ".btn_delete", function() {
                let id = $(this).data('id')
                Swal.fire({
                    icon: 'question',
                    title: 'Yakin Menghapus Kode Spesial?',
                    showCancelButton: true,
                    confirmButtonText: `Ya, Hapus!`,
                    cancelButtonText: `Batal.`,
                    confirmButtonColor: '#FF0000'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.replace(URL.delete.replace('__id', id));
                    }
                })

            })

            @if ($errors->any())
                @if (Session::get('condition') == 'storeSpecialCode')
                    openModal({
                    url: URL.addUser,
                    title: 'Tambah User'
                    })
                @endif
            @endif
            @if (Session::get('icon'))
                Swal.fire({
                title: "{{ Session::get('title') }}",
                text: "{{ Session::get('text') }}",
                icon: "{{ Session::get('icon') }}"
                })
            @endif
        })
    </script>
@endsection
