@extends('master')

@section('css_after')
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('content')
    <div class="container-fluid my-4">
        <div class="row d-flex">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-header d-flex">
                        <h2>List Pengguna</h2>
                        <button class="btn btn-sm btn-primary ml-auto" id="add_button"><i class="fas fa-plus"></i>
                            Tambah</button>
                        <button class="btn btn-sm btn-success ml-2" id="transfer_button"><i class="fa fa-exchange"
                                aria-hidden="true"></i></i>
                            Transfer</button>
                    </div>
                    <div class="card-body">
                        <table id="user_table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Kode</th>
                                    <th>Level</th>
                                    <th>Terakhir Diupdate</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['users'] as $user)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->kode }}</td>
                                        <td>{{ $user->role->name }} </td>
                                        <td>{{ \Carbon\Carbon::parse($user->updated_at)->format('d.m.Y h:i:s') }}</td>
                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <button class="btn btn-sm btn-success btn_edit mr-2" data-toggle="tooltip"
                                                    data-placement="top" title="Edit Data" data-id="{{ $user->id }}">
                                                    <i class=" fas fa-edit"></i>
                                                </button>
                                                <button class="btn btn-sm btn-danger btn_delete"
                                                    data-id="{{ $user->id }}" data-toggle="tooltip" data-placement="top"
                                                    title="Hapus Data">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Kode</th>
                                    <th>Level</th>
                                    <th>Terakhir Diupdate</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal tambah/edit -->
    <div class="modal fade" id="main_modal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="main_form" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                name="name" placeholder="Masukkan nama pengguna" value="{{ old('name') }}">
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                                name="email" placeholder="Masukkan email pengguna" value="{{ old('email') }}">
                            @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="phone">Nomer Telfon</label>
                            <input type="number" class="form-control @error('phone') is-invalid @enderror" id="phone"
                                name="phone" placeholder="Masukkan nomer telpon pengguna" value="{{ old('phone') }}">
                            @error('phone')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="city_of_birth">Tempat Lahir</label>
                            <input type="text" class="form-control @error('city_of_birth') is-invalid @enderror"
                                id="city_of_birth" name="city_of_birth" placeholder="Masukkan tempat lahir pengguna"
                                value="{{ old('city_of_birth') }}">
                            @error('city_of_birth')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <label for="birth">Tanggal Lahir</label>
                        <div class="input-group date">
                            <input id="birth" name="birth" type="text" placeholder="Masukkan tanggal lahir"
                                class="form-control @error('birth') is-invalid @enderror" value="{{ old('birth') }}">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                            </div>

                        </div>
                        @error('birth')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label for="" class="mb-0 mt-3">Foto KTP</label>
                        <div class="row">
                            <div class="col-md-5">
                                <img id="ktp_preview" class="img-fluid" src="" alt="your image" />
                            </div>
                            <div class="col-md-7 d-flex">
                                <div class="form-group my-auto">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input @error('ktp') is-invalid @enderror"
                                            id="ktp" name="ktp">
                                        <label id="ktp_label" class="custom-file-label" for="ktp">Pilih foto</label>
                                        @error('ktp')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <small class="form-text text-muted">- Harus berupa gambar
                                            (format: jpg, jpeg, svg,
                                            png , dll)</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="level">Level User</label>
                            <select class="custom-select @error('role_id') is-invalid @enderror" name="role_id" id="level">
                                <option @if (old('role_id') == '') selected @endif disabled value="">Pilih Level</option>
                                @foreach ($data['roles'] as $role)
                                    <option @if (old('role_id') == $role->id) selected @endif value="{{ $role->id }}">{{ $role->name }}
                                        ({{ $role->min }} - {{ $role->max }})
                                    </option>
                                @endforeach
                            </select>
                            @error('role_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="kode">Kode</label>
                            <input type="number" class="form-control @error('kode') is-invalid @enderror" id="kode"
                                name="kode" placeholder="Masukkan kode pengguna" value="{{ old('kode') }}">
                            @error('kode')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="transfer_modal" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Transfer Kode User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('user.transfer') }}" id="transfer_form" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Pilih User Sumber</label>
                            </div>
                            <div class="col-md-12">
                                <select class="select2" id="source" name="source" style="width: 100%" required>
                                    <option value="" disabled @if (old('source') == '') selected @endif>Pilih User</option>
                                    @foreach ($data['users'] as $user)
                                        <option value="{{ $user->id }}" @if (old('source') == $user->id) selected @endif>
                                            {{ $user->name }} - {{ $user->kode }}

                                        </option>
                                    @endforeach
                                </select>
                                @error('source')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="">Pilih User Tujuan</label>
                            </div>
                            <div class="col-md-12">
                                <select class="select2" id="destination" name="destination" style="width: 100%" required>
                                    <option value="" disabled @if (old('destination') == '') selected @endif>Pilih User</option>
                                    @foreach ($data['users'] as $user)
                                        <option value="{{ $user->id }}" @if (old('destination') == $user->id) selected @endif>
                                            {{ $user->name }} - {{ $user->kode }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('destination')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-12 mt-3">
                                <div class="form-group">
                                    <label for="kode">Kode Baru User Sumber</label>
                                    <input type="number" class="form-control @error('kode') is-invalid @enderror"
                                        name="kode" placeholder="Masukkan kode pengguna" value="{{ old('kode') }}">
                                    @error('kode')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <small class="form-text text-muted">Kode harus sesuai dengan aturan level user.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
        integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        const URL = {
            addUser: "{{ route('user.store') }}",
            getDetail: "{{ route('user.detail', ['__id']) }}",
            update: "{{ route('user.update', ['__id']) }}",
            delete: "{{ route('user.delete', ['__id']) }}",
            defaultImage: "{{ asset('images/picture.svg') }}"
        }
    </script>

    <script>
        function openModal(data) {
            $("#main_form").attr('action', data.url)
            $("#modal_title").html(data.title)
            if (data.hasOwnProperty('data')) {
                $("#name").val(data.data.name)
                $("#email").val(data.data.email)
                $("#level").val(data.data.role_id)
                $("#kode").val(data.data.kode)
                $("#phone").val(data.data.phone)
                $("#city_of_birth").val(data.data.city_of_birth)
                $("#birth").val(data.data.birth)
                $('#ktp_preview').attr('src', data.data.ktp)
                $('#ktp_label').html('Pilih untuk merubah KTP');
            } else {
                $('#ktp_preview').attr('src', URL.defaultImage)
            }
            if (data.resetInput) {
                $("#name").val('')
                $("#email").val('')
                $("#level").val('')
                $("#kode").val('')
                $("#phone").val('')
                $("#city_of_birth").val('')
                $("#birth").val('')
                $('#ktp_preview').attr('src', URL.defaultImage)
                $('#ktp').val('')
                $('#ktp_label').html('Pilih Foto');
            }
            $("#main_modal").modal('show')
        }

        $(document).ready(function() {
            $('#user_table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
            $('.select2').select2();
            $('.date').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $("#kode").mask('9999');

            $("#add_button").click(function() {
                openModal({
                    url: URL.addUser,
                    title: 'Tambah User',
                    resetInput: true
                })
            })

            $(document).on('click', ".btn_edit", function() {
                let id = $(this).data('id')
                $("#main_loading").show();
                fetch(URL.getDetail.replace('__id', id))
                    .then(resp => resp.json())
                    .then(data => {
                        openModal({
                            url: URL.update.replace('__id', id),
                            title: 'Edit User',
                            data: data.user
                        })
                    })
                    .then(() => {
                        $("#main_loading").hide();
                    })
                    .catch(() => {
                        Swal.fire({
                            title: "Error!",
                            text: "Data user tidak ditemukan.",
                            icon: "error"
                        })
                    })
            })

            $(document).on('click', ".btn_delete", function() {
                let id = $(this).data('id')
                Swal.fire({
                    icon: 'question',
                    title: 'Yakin Menghapus User?',
                    showCancelButton: true,
                    confirmButtonText: `Ya, Hapus!`,
                    cancelButtonText: `Batal.`,
                    confirmButtonColor: '#FF0000'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.replace(URL.delete.replace('__id', id));
                    }
                })

            })

            function readURL(input) {
                // let label =
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#ktp_preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                    $('#ktp_label').html($("#ktp")[0].files[0].name);
                }
            }

            $("#ktp").change(function() {
                readURL(this);
            });

            $("#transfer_button").click(function() {
                $("#transfer_modal").modal("show");
            })

            $("form").submit(function() {
                $("#main_loading").show();
            })
            $("#transfer_form").submit(function() {
                if ($("#source").val() == $("#destination").val()) {
                    $("#main_loading").hide();
                    event.preventDefault()
                    Swal.fire({
                        title: "Error!",
                        text: "User yang dipilih tidak boleh sama.",
                        icon: "error"
                    })
                }
            })


            @if ($errors->any())
                @if (Session::get('condition') == 'storeUser')
                    openModal({
                    url: URL.addUser,
                    title: 'Tambah User'
                    })
                @elseif (Session::get('condition') == 'updateUser')
                    openModal({
                    url: URL.update.replace('__id', {{ Session::get('updatedId') }}),
                    title: 'Edit User',
                    })
                @elseif (Session::get('condition') == 'transfer')
                    $("#transfer_modal").modal("show");
                @endif
            @endif
            @if (Session::get('icon'))
                Swal.fire({
                title: "{{ Session::get('title') }}",
                text: "{{ Session::get('text') }}",
                icon: "{{ Session::get('icon') }}"
                })
            @endif
        })
    </script>
@endsection
