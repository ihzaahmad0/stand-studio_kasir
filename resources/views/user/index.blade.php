@extends('user.master')

@section('css_after')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('content')
    <div class="container bootstrap snippet">
        <div class="row" style="display: flex">
            <div class="col-sm-10 ">
                <h1>Profile User</h1>
            </div>
            <div class="col-sm-2" style="margin-top: auto;
                    margin-bottom: auto;">
                <a class="pull-right" href="{{ route('logout') }}">Logout</a>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <!--left col-->

                <form class="form" action="{{ route('guard.user.update') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="text-center">
                        <img src="{{ Auth::user()->ktp }}" class="avatar img-circle img-thumbnail" alt="avatar">
                        <input type="file" class="text-center center-block file-upload" name="ktp">
                        @error('ktp')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    </hr><br>


                    {{-- <div class="panel panel-default">
                    <div class="panel-heading">Website <i class="fa fa-link fa-1x"></i></div>
                    <div class="panel-body"><a href="http://bootnipets.com">bootnipets.com</a></div>
                </div>


                <ul class="list-group">
                    <li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i></li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Shares</strong></span> 125</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Likes</strong></span> 13</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Posts</strong></span> 37</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Followers</strong></span> 78</li>
                </ul> --}}

                    {{-- <div class="panel panel-default">
                    <div class="panel-heading">Social Media</div>
                    <div class="panel-body">
                        <i class="fa fa-facebook fa-2x"></i> <i class="fa fa-github fa-2x"></i> <i
                            class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i> <i
                            class="fa fa-google-plus fa-2x"></i>
                    </div>
                </div> --}}

            </div>
            <!--/col-3-->
            <div class="col-sm-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Profile</a></li>
                    @if (Auth::user()->role_id <= 2)
                        <li><a data-toggle="tab" href="#vvip">Menu VVIP</a></li>
                    @endif
                    @if (Auth::user()->role_id <= 3)
                        <li><a data-toggle="tab" href="#vip_a">Menu VIP A</a></li>
                    @endif
                    @if (Auth::user()->role_id <= 4)
                        <li><a data-toggle="tab" href="#vip_b">Menu VIP B</a></li>
                    @endif
                    @if (Auth::user()->role_id <= 5)
                        <li><a data-toggle="tab" href="#gold">Menu Gold</a></li>
                    @endif
                    @if (Auth::user()->role_id <= 6)
                        <li><a data-toggle="tab" href="#silver">Menu Silver</a></li>
                    @endif
                    @if (Auth::user()->role_id <= 7)
                        <li><a data-toggle="tab" href="#bronze">Menu Bronze</a></li>
                    @endif

                </ul>


                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <hr>

                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="first_name">
                                    <h4>Nama</h4>
                                </label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                    id="first_name" placeholder="Nama User" value="{{ Auth::user()->name }}">
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="email">
                                    <h4>Email</h4>
                                </label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                    id="email" placeholder="you@email.com" value="{{ Auth::user()->email }}">
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="phone">
                                    <h4>Telpon</h4>
                                </label>
                                <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone"
                                    id="phone" placeholder="enter phone" value="{{ Auth::user()->phone }}">
                                @error('phone')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="email">
                                    <h4>Kode</h4>
                                </label>
                                <input type="text" class="form-control" id="location" placeholder="somewhere"
                                    value="{{ Auth::user()->kode }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="password">
                                    <h4>Tempat Lahir</h4>
                                </label>
                                <input type="text" class="form-control @error('city_of_birth') is-invalid @enderror"
                                    name="city_of_birth" id="password" value="{{ Auth::user()->city_of_birth }}">
                                @error('city_of_birth')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <label for="password2">
                                <h4>Tanggal Lahir</h4>
                            </label>
                            <div class="input-group date" style="width: 100%">
                                <input id="birth" name="birth" type="text" placeholder="Masukkan tanggal lahir"
                                    class="form-control @error('birth') is-invalid @enderror"
                                    value="{{ Auth::user()->birth }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                                </div>
                                @error('city_of_birth')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-success" type="submit"><i
                                        class="glyphicon glyphicon-ok-sign"></i> Simpan</button>
                            </div>
                        </div>
                        </form>

                        <hr>

                    </div>
                    <!--/tab-pane-->
                    <div class="tab-pane" id="vvip">

                        <h2>INI ADALAH MENU VVIP => hanya bisa di akses oleh VVIP</h2>

                        <hr>

                    </div>
                    <!--/tab-pane-->
                    <div class="tab-pane" id="vip_a">

                        <h2>INI ADALAH MENU VIP A => hanya bisa di akses oleh VIP A dan lebih tinggi</h2>

                        <hr>

                    </div>
                    <div class="tab-pane" id="vip_b">

                        <h2>INI ADALAH MENU VIP B => hanya bisa di akses oleh VIP B dan lebih tinggi</h2>

                        <hr>

                    </div>
                    <div class="tab-pane" id="gold">

                        <h2>INI ADALAH MENU GOLD => hanya bisa di akses oleh GOLD dan lebih tinggi</h2>

                        <hr>

                    </div>
                    <div class="tab-pane" id="silver">

                        <h2>INI ADALAH MENU SILVER => hanya bisa di akses oleh SILVER dan lebih tinggi</h2>

                        <hr>

                    </div>
                    <div class="tab-pane" id="bronze">

                        <h2>INI ADALAH MENU BRONZE => hanya bisa di akses oleh BRONZE dan lebih tinggi</h2>

                        <hr>

                    </div>

                </div>
                <!--/tab-pane-->
            </div>
            <!--/tab-content-->

        </div>
        <!--/col-9-->
    </div>
    <!--/row-->
@endsection

@section('js_after')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(document).ready(function() {
            $('.date').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });

            var readURL = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.avatar').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }


            $(".file-upload").on('change', function() {
                readURL(this);
            });
        });
    </script>
    @if (Session::get('icon'))
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            Swal.fire({
                title: "{{ Session::get('title') }}",
                text: "{{ Session::get('text') }}",
                icon: "{{ Session::get('icon') }}"
            })
        </script>
    @endif
@endsection
